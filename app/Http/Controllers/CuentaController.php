<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cuenta;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class CuentaController extends BaseController
{
#Listar todos los clientes
    public function index(Request $request){
   		$cuentas = Cuenta::all();
   		return response()->json($cuentas, 200);
    }
    
    public function getCuenta(Request $request, $numero){
        if ($request->isjson()){
            $cuentas = Cuenta::where('numero', $numero)->get();
            #if (!$cliente->isEmpty()){
            if(empty($cuentas)){
             $status = false;
             $info = 'Data is not listed successfully';
         }
         else{
             $status = true;
             $info = 'Data is listed successfully';   			
         }
     return ResponseBuilder::result($status, $info, $cuentas);
        }
        else{
                $status = false;
             $info = 'Unauthorized';
        return ResponseBuilder::result($status, $info);
        }
    }

    public function createCuenta(Request $request){
        $cliente = new Cliente();
        $cuenta = new Cuenta();
        #Generar número de cuenta
        $Numero1=rand(9999999, 99999999);
        $Numero2=rand(9999999, 99999999);
        $Numero=$Numero1*$Numero2;
        $cuenta->numero = $Numero;
        $cuenta->estado = '1';
        $cuenta->fechaApertura = $request->fechaApertura;
        $cuenta->tipoCuenta = $request->tipoCuenta;
        $cuenta->saldo = $request->saldo;
        $cuenta->cliente_id = $cliente->id;
        $cuenta->save();
        return response()->json($cliente);
 }

}
