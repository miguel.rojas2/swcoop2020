<?php

namespace App\Http\Controllers;
use App\Cliente;
use App\Cuenta;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;


class ClienteController extends BaseController
{
   
		#este metdo sirve para poder llamar a todos los datos q teng registrado dentro de la base de datos
    public function index(Request $request){

    	#if($request->isJson()){

	    	$cliente =Cliente::all();
	    	if(!$cliente->isEmpty()){
	    		$status=true;
	    		$info="Data is listed successfully";

	    	}else 	{
	    		$status=false;
	    		$info="Data not listed successfully";

	    	}
	    	return response()->json($cliente,200);
	 #   }else{
	  #  	$status=false;
	   # 	$info="Unauthorized";
	   #}
	    #return responseBuilder::result($status,$info);
	    
    }
    #busco este emtodo por un get q me sirve para buscar solo con el numero de cedula

   public function getCliente(Request $request, $cedula)
   {
    	$cliente = Cliente::where('cedula',$cedula)->get();

	    	if($request -> isJson())
	    	{
		    	
		    	return response()->json($cliente,200);
		    	if($cliente -> isEmpty())
		    	{
		    		$status = true;
		    		$info = "DAta is listed successfully";

		    	}else
		    	{
		    		$status = false;
		    		$info = "DAta is not listed successfully";
		    	}
		    	return ResponseBuilder::result($status, $info, $cliente);	
		    	
	    	}else
	    	{
	    		$status = false;
	    		$info = 'Desautorizado';
	    	}
	    	return ResponseBuilder::result($status, $info, $cliente);
	}
	
    
    #esta es una funcion va hacer de tipo POST y es la de crear un cliente, tambien se crea la cuenta
  #Crear un cliente
	public function createCliente(Request $request){
   		$cliente = new Cliente();
		   $cuenta = new Cuenta();
		   echo $request->cedula;
   		$cliente->cedula = $request->cedula;
   		$cliente->nombres = $request->nombres;
   		$cliente->apellidos = $request->apellidos;
   		$cliente->genero = $request->genero;
   		$cliente->estadoCivil = $request->estadoCivil;
   		$cliente->fechaNacimiento = $request->fechaNacimiento;
   		$cliente->correo = $request->correo;
		$cliente->telefono = $request->telefono;
		$cliente->celular = $request->celular;
		$cliente->direccion = $request->direccion;		
	   	$cliente->save();
   		#Generar número de cuenta
   		$Numero1=rand(9999999, 99999999);
		$Numero2=rand(9999999, 99999999);
		$Numero=$Numero1*$Numero2;
		#
   		$cuenta->numero = $Numero;
   		$cuenta->estado = '1';
   		$cuenta->fechaApertura = $request->fechaApertura;
   		$cuenta->tipoCuenta = $request->tipoCuenta;
   		$cuenta->saldo = $request->saldo;
   		$cuenta->cliente_id = $cliente->id;
   		$cuenta->save();
   		return response()->json($cliente);
	}
	
	
	public function ModificarCliente(Request $request, $cedula){
		$cliente = Cliente::where('cedula', $cedula)->first();
		 if(empty($cliente)){
		   $status = false;
		   $info = 'Data is not in the list';
		}
		else{
		   #Modificación de datos
		   $cliente->nombres = $request->nombres;
		   $cliente->apellidos = $request->apellidos;  
		   $cliente->save();          
		}
  }
    



}
    
     
   

	
